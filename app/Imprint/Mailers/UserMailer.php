<?php namespace Imprint\Mailers;

use User;

class UserMailer extends Mailer {

    public function welcome(User $user)
    {
        $view = 'emails.welcome';
        $data = [];
        $subject = 'Welcome to Imprint!';

        return $this->sendTo($user, $subject, $view, $data);
    }

}

<?php namespace Imprint\Services;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(
            'Imprint\Repos\UserRepositoryInterface',
            'Imprint\Repos\DbUserRepository'
        );

        $this->app->bind(
            'Imprint\Services\Validation\Validator',
            'Imprint\Services\Validation\UserValidator'

        );
    }
}

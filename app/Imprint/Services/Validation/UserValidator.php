<?php namespace Imprint\Services\Validation;

/**
* 
*/
class UserValidator extends Validator
{
        static $rules = [
            'email' => 'required|unique:users,email',
            'password' => 'required'
        ];
}
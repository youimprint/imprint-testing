<?php namespace Imprint\Services\Validation;

use Validator as V;

/**
* 
*/
abstract class Validator
{
    protected $errors;

    public function getErrors()
    {
        return $this->errors;
    }
    
    public function isValid(array $input)
    {
        $v = V::make($input, static::$rules);

        if ($v->fails())
        {
            $this->errors = $v->messages();

            return false;
        }

        return true;
    }

    public function errors()
    {
        return $this->errors;
    }
}
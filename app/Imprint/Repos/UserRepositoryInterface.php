<?php namespace Imprint\Repos;

interface UserRepositoryInterface
{
    public function createUser(array $data);
}

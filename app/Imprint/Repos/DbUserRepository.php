<?php namespace Imprint\Repos;

use Imprint\Exceptions\ValidationException;
use Imprint\Services\Validation\UserValidator;    
use User;
use Hash;
use Auth;
use Input;

class DbUserRepository implements UserRepositoryInterface
{

    protected $validator;

    public function __construct(UserValidator $validator)
    {
        $this->validator = $validator;
    }

    public function createUser(array $data)
    {
        if ($this->validator->isValid($data))
        {
            $user = new User;
            $user->email = $data['email'];
            $user->password = Hash::make($data['password']);
            $user->save();

            Auth::login($user);

            return true;
        }

        throw new ValidationException('Validation Failed', $this->validator->getErrors());
    }
}

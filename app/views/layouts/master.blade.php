<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <style>
        @import url(//fonts.googleapis.com/css?family=Lato:300,400,700);

        .welcome {
           width: 300px;
           height: 300px;
           position: absolute;
           left: 50%;
           top: 50%; 
           margin-left: -150px;
           margin-top: -150px;
        }

        .flash-message{
          border: 1px dotted black;
          background-color:#e3e3e3;
        }
    </style>
    @assets('site.css', 'site.js')
</head>
<body>
    <div class="row">

      @if (Session::has('flash_message'))

      <div class="flash-message">
        <span>{{ Session::get('flash_message') }}</span>
      </div>

      @endif

@yield('body')
    </div>
</body>
</html>

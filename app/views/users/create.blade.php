@extends('layouts.master')

@section('title')
Imprint | Register
@stop

@section('body')
<div class="col-lg-3 col-lg-offset-5 regFormBox">
<span><h3>Register</h3></span>
{{Form::open(array('route' => 'users.store')) }}

    <div class="form-group">
        {{Form::label('email', 'Email:', array(
            'for' => 'email'
        )) }}

        {{Form::email('email', null, array(
            'class' => 'form-control',
            'id' => 'email',
            'placeholder' => 'Enter email'
        )) }}

        {{$errors->first('email', '<span class="error">:message</span>') }}
    </div>

    <div class="form-group">
        {{Form::label('password', 'Password:', array(
            'for' => 'password'
        )) }}

        {{Form::password('password', array(
            'class' => 'form-control',
            'id' => 'password',
            'placeholder' => 'Password'
        )) }}

         {{$errors->first('password', '<span class="error">:message</span>') }}
    </div>

        {{Form::submit('Submit', array(
            'class' => 'btn btn-default'
        )) }}


{{Form::close() }}
</div>
@stop
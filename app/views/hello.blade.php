<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Imprint | Beta</title>
    <style>
        @import url(//fonts.googleapis.com/css?family=Lato:300,400,700);

        .welcome {
           width: 300px;
           height: 300px;
           position: absolute;
           left: 50%;
           top: 50%; 
           margin-left: -150px;
           margin-top: -150px;
        }
    </style>
<!--     @assets('site.css', 'site.js') -->
</head>
<body>
    <div class="welcome">
        {{HTML::image('assets/images/logo.jpg');}}
    </div>
</body>
</html>
